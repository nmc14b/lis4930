> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4930 Requirements
# Nathaniel Chin
*Course Work Links*

1. [A1 README.md](assignments/ass1/README.md)

* Screenshot of running JDK java Hello (#2 above);
* Screenshot of running Android Studio My First App (#2 above, example below);
* Screenshot of running Android Studio Contacts App (example below);
* git commands w/short descriptions;
* Bitbucket repo links: a) this assignment and b) the completed tutorial repos above (bitbucketstationlocations and myteamquotes).

2. [A2 README.md](assignments/ass2/README.md)

* Screenshot of running application's unpopulated user interface;
* Screenshot of running application's populated user interface;

3. [A3 README.md](assignments/ass3/README.md)

* Screenshot of running application’s unpopulated user interface;
* Screenshot of running application’s toast notification;
* Screenshot of running application’s converted currency user interface;

4. [Pro 1.md](assignments/pro1/README.md)

* Screenshot of running application’s splash screen;
* Screenshot of running application’s follow-up screen (with images and buttons);
* Screenshots of running application’s play and pause user interfaces (with images and buttons);

5. [A4.md](assignments/ass4/README.md)

* Screenshot of running application’s splash screen;
* Screenshot of running application’s invalid screen (with appropriate image);
* Screenshots of running application’s valid screen (with appropriate image);

6. [A5.md](assignments/ass5/README.md)

* Screenshot of running application’s splash screen (list of articles – activity_items.xml);
* Screenshot of running application’s individual article (activity_item.xml);
* Screenshots of running application’s default browser (article link);


