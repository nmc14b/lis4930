> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# lis4930

## Nathaniel Chin

### Assignment 5 Requirements:
> *Sub-Heading:*

1. Include splash screen with app title and list of articles.
2. Must find and use your own RSS feed.
3. Must add background color(s) or theme.
4. Create and display launcher icon image.

#### README.md file should include the following items:

* Screenshot of running application’s splash screen (list of articles – activity_items.xml);
* Screenshot of running application’s individual article (activity_item.xml);
* Screenshots of running application’s default browser (article link);

#### Assignment Screenshots:

*Screenshot of running application’s splash screen (list of articles – activity_items.xml)*:

![application, splash screen](img/1.png)

*Screenshot of running application’s individual article (activity_item.xml)*:

![individual article](img/2.png)

*Screenshots of running application’s default browser (article link);*:

![article in browser](img/3.png)
