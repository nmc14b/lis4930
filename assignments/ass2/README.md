> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# lis4930

## Nathaniel Chin

### Assignment # Requirements:
> *Sub-Heading:*

1. Drop-down menu for total number of guests (including yourself): 1 – 10
2. Drop-down menu for tip percentage (5% increments): 0 – 25
3. Must add background color(s) or theme
4. Extra Credit (10 pts): Create and display launcher icon image

#### README.md file should include the following items:

* Screenshot of running application's **unpopulated** user interface;
* Screenshot of running application's **populated** user interface;

#### Assignment Screenshots:

*Screenshot of running application's **unpopulated** user interface*:

![application, unpopulated](img/tipCalculator1.png)

*Screenshot of running application's **populated** user interface*:

![application, populated](img/tipCalculator2.png)


