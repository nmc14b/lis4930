> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# lis4930

## Nathaniel Chin

### Assignment 4 Requirements:

*Sub-Heading:*

1. Course title, your name, assignment requirements, as per A1;
2. Screenshot of running application’s splash screen;
3. Screenshot of running application’s invalid screen (with appropriate image);
4. Screenshots of running application’s valid screen (with appropriate image);

#### README.md file should include the following items:

* Bullet-list items
* Screenshot of running application’s splash screen;
* Screenshot of running application’s invalid screen (with appropriate image);
* Screenshots of running application’s valid screen (with appropriate image);


#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![Splash Screen](img/splash_screen.png)

*Screenshot of running java Hello*:

![Invalid Entry](img/invalid_entry.png)

*Screenshot of Android Studio - My First App*:

![Valid Entry](img/valid_entry.png)
