> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4930

## Nathaniel Chin

### Project 1 Requirements:

*Three Parts:*

1. Research how to complete the following requirements (see screenshots below):
	* Include splash screen image, app title, intro text.
	* Include artists’ images and media.
	* Images and buttons must be vertically and horizontally aligned.
	* Must add background color(s) or theme
	* Create and display launcher icon image
2. Screenshots of the Music Player Application
3. Chapter 5 Questions

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1;
* Screenshot of running application’s splash screen;
* Screenshot of running application’s follow-up screen (with images and buttons);
* Screenshots of running application’s play and pause user interfaces (with images and buttons);


#### Assignment Screenshots:

*Screenshot of running java Hello*:

![Splash Screen](img/p1_1.png)

*Screenshot of running application’s splash screen:*

![Music Player Interface](img/p1_2.png)

*Screenshot of running application’s follow-up screen (with images and buttons)*

![Music Player - Playing Song](img/p1_3.png)

*Screenshots of running application’s play and pause user interfaces (with images and buttons)*

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/nmc14b/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/nmc14b/myteamquotes/ "My Team Quotes Tutorial")



