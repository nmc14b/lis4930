> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# lis4930

## Nathaniel Chin

### Assignment 3 Requirements:

*Sub-Heading:*

1. Course title, your name, assignment requirements, as per A1;
2. Screenshot of running application’s unpopulated user interface;
3. Screenshot of running application’s toast notification;
4. Screenshot of running application’s converted currency user interface;

#### README.md file should include the following items:

* Bullet-list items
* Screenshot of running application’s unpopulated user interface;
* Screenshot of running application’s toast notification;
* Screenshot of running application’s converted currency user interface;


#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![Home Screen](img/ss1.png)

*Screenshot of running java Hello*:

![Error message](img/ss2.png)

*Screenshot of Android Studio - My First App*:

![Clean Run](img/ss3.png)
