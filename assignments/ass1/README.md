> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# lis4930

## Nathaniel Chin

### Assignment # Requirements:

*Sub-Heading:*

1. Distributed Version Control with Git and Bitbucket, and Development Environments
2. Development Installation
3. Questions

#### README.md file should include the following items:

* Screenshot of running JDK java Hello (#2 above);
* Screenshot of running Android Studio My First App (#2 above, example below);
* Screenshot of running Android Studio Contacts App (example below);
* git commands w/short descriptions;
* Bitbucket repo links: a) this assignment and b) the completed tutorial repos above (bitbucketstationlocations and myteamquotes).

README.md file should include the following items:
a. Main screen with 3 buttons, and contact information w/image for at least 3 contacts.
b. *Must* include background color and design—cannot use “default” activity design.
c. *Be sure* to review Chs. 1 and 2 of the course textbook.

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init: Create an empty Git repository or reinitialize an existing one
2. git status: Show the working stree status
3. git add: Add file contents to the index
4. git commit: Record changes to the repository
5. git push: Update remote refs along with associated objects
6. git pull: Fetch from and integrate with another repository or a local branch
7. git clone: Clone a repository

#### Assignment Screenshots:

*Screenshot of running java Hello*:

![Hello World!](img/javaHello.png)

*Screenshot of Android Studio - My First App*:

![Android Studio My First App](img/myFirstApp.png)

> *Screenshot of Android Studio - Contacts App Main Screen*

> ![Android Studio Contacts App Main Screen](img/contact_mainScreen.png)

> *Screenshot of Android Studio - Contacts App Contact 1 Info*

> ![Android Studio Contacts App Contact 1](img/contact_c1.png)

> *Screenshot of Android Studio - Contacts App Contact 2 Info*

> ![Android Studio Contacts App Contact 2](img/contact_c2.png)

> *Screenshot of Android Studio - Contacts App Contact 3 Info*

> ![Android Studio Contacts App Contact 3](img/contact_c3.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/nmc14b/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/nmc14b/myteamquotes/ "My Team Quotes Tutorial")



